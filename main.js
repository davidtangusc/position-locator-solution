var map = new google.maps.Map(document.getElementById('map-canvas'), {
	center: { lat: -34.397, lng: 150.644 },
	zoom: 5
});

var locator = {
	getMyPosition: function(callback) {
  	var error_handler = function(err) {
  		alert('Aww, snap! Something went wrong.');
  	};

  	navigator.geolocation.getCurrentPosition(callback, error_handler);
	}
};

locator.getMyPosition(function(position) {
	var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	var marker = new google.maps.Marker({
		map: map,
		position: latlng,
		icon: 'kitten.png',
		animation: google.maps.Animation.BOUNCE,
	});

	var infowindow = new google.maps.InfoWindow({
		content: 'Meow',
		position: latlng
	});

	map.setCenter(latlng);

	google.maps.event.addListener(marker, 'click', function(event) {
		infowindow.open(map);
	});

	var geocoder = new google.maps.Geocoder();

	geocoder.geocode({
		location: latlng
	}, function(results) {
		infowindow.setContent(results[0].formatted_address);
	});
});
